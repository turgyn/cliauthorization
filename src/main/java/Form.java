import java.util.Scanner;

public class Form {
    // Fake hasher
    private static String hashPass(String pass) {
        int hashCode = Math.abs(pass.hashCode());
        StringBuilder builder = new StringBuilder();
        while (hashCode > 0) {
            int p = hashCode % 62;
            hashCode /= 62;
            char cur;
            if (p < 26) cur = (char) ('A' + p);
            else if (p < 52) cur = (char) ('a' + p - 26);
            else cur = (char) ('0' + p - 52);
            System.out.println(cur);
            builder.append(cur);
        }
        return builder.toString();
    }

    private static boolean emailValidation(String email) {
        String pattern = "^(.+)@(.+)$";
        return email.matches(pattern);
    }

    private static boolean passwordValidation(String lhs, String rhs) {
        String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
        return lhs.equals(rhs) && lhs.matches(pattern);
    }

    public static User authenticationForm(Scanner scanner) {
        User user = new User();
        System.out.print("Your username: ");
        user.setUserName(scanner.next());
        System.out.print("Your password: ");
        user.setPassoword(hashPass(scanner.next()));
        return user;
    }

    public static User authorizationForm(Scanner scanner) {
        User user = new User();
        System.out.print("Username: ");
        user.setUserName(scanner.next());
        while (true) {
            System.out.print("Email address: ");
            String email = scanner.next();
            if (emailValidation(email)) {
                user.setEmail(email);
                break;
            } else {
                System.out.println("Wrong email format: it should contain '@'");
            }
        }
        while (true) {
            System.out.print("Your password: ");
            String pass1 = scanner.next();
            System.out.print("Confirm password: ");
            String pass2 = scanner.next();
            if (passwordValidation(pass1, pass2)) {
                user.setPassoword(hashPass(pass1));
                break;
            } else {
                System.out.println("Different passwords or wrong format:\n" +
                        "Minimum length: 8\n" +
                        "Minimum 1 lower case symbol\n" +
                        "Minimum 1 upper case symbol\n" +
                        "Minimum 1 specific symbol\n" +
                        "Minimum 1 digit\n");
            }
        }
        return user;
    }
}
