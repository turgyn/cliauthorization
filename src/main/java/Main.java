import java.util.Scanner;

public class Main {
    static final String DATABASE_URL = "jdbc:postgresql://localhost:5432/authorize_db";
    static final String USER = "darustur";
    static final String PASSWORD = "darustur";

    public static void main(String[] args) {
        DbManager dbManager = new DbManager(DATABASE_URL, USER, PASSWORD);
        Scanner scanner = new Scanner(System.in);

        dbManager.connect();
        dbManager.createTableIfNotExists();

        String starterMessage = "Hi, enter action number.\n1: Log in.\n2: Sign up\n3: Clear\n4: Exit";
        boolean alive = true;

        while (alive) {
            System.out.println(starterMessage);
            int action = scanner.nextInt();
            String messageResponse = "";
            if (action == 1) {
                User user = Form.authenticationForm(scanner);
                messageResponse = dbManager.authentication(user);
            } else if (action == 2) {
                User user = Form.authorizationForm(scanner);
                int response = dbManager.authorization(user);
                switch (response) {
                    case 0:
                        messageResponse = "User successfully created";
                        break;
                    case 1:
                        messageResponse = "User with such login already exists";
                        break;
                    case 2:
                        messageResponse = "User with such email already exists";
                        break;
                    case 3:
                        messageResponse = "Some sql error occurs";
                        break;
                }
            } else if (action == 3) {
                dbManager.clearTable();
                messageResponse = "Done";
            } else if (action == 4) {
                alive = false;
                messageResponse = "Bye(";
            } else {
                messageResponse = "Error";
            }
            System.out.println(messageResponse);
        }
    }
}
