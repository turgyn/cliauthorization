import java.sql.*;

public class DbManager {
    private Connection connection;
    private Statement statement;
    private String url;
    private String user;
    private String password;

    DbManager(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public void connect() {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, user, password);
            statement = connection.createStatement();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public void createTableIfNotExists() {
        String sql = "CREATE TABLE IF NOT EXISTS users (" +
                "username VARCHAR(50) PRIMARY KEY," +
                "email VARCHAR(50) NOT NULL UNIQUE," +
                "password VARCHAR(50) NOT NULL );";
        try {
            statement.execute(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public int authorization(User user) {
        String raw = "SELECT username FROM users WHERE %s = '%s';";
        try {
            String sql = String.format(raw, "username", user.getUserName());
            if (statement.executeQuery(sql).next()) {
                return 1;
            }
            sql = String.format(raw, "email", user.getEmail());
            if (statement.executeQuery(sql).next()) {
                return 2;
            }
            sql = String.format(
                    "INSERT INTO users (username, email, password) VALUES ('%s', '%s', '%s');",
                    user.getUserName(), user.getEmail(), user.getPassoword());
            statement.execute(sql);
        } catch (SQLException throwables) {
            return 3;
        }
        return 0;
    }

    public String authentication(User user) {
        String sql = String.format(
                "SELECT * FROM users WHERE username = '%s';", user.getUserName()
        );
        ResultSet rs;
        try {
            rs = statement.executeQuery(sql);
            if (!rs.next()) {
                return "User with such username not found";
            } else if (!rs.getString("password").equals(user.getPassoword())) {
                return "Wrong password";
            } else {
                return rs.getString("email");
            }
        } catch (SQLException e) {
            return e.toString();
        }
    }

    public void clearTable() {
        String sql = "DELETE FROM users";
        try {
            statement.execute(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
