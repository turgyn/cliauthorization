import java.util.List;

public class User {
    private String userName;
    private String email;
    private String passoword;

    public User() {}

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassoword() {
        return passoword;
    }

    public void setPassoword(String passoword) {
        this.passoword = passoword;
    }

    public String toString() {
        return userName + "\t" + email + "\t" + passoword + "\n";
    }

    public static String prettyUsersInfo(List<User> users) {
        StringBuilder res = new StringBuilder("id\tfirst_name\tlast_name\tbirth_date\tgender\n");
        for (User use: users) {
            res.append(use).append("\n");
        }
        return res.toString();
    }
}
